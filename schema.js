const fetch = require('node-fetch');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLSchema,
	GraphQLList
} = require('graphql');

const OPTIONS = {
	headers: {
		'user-key':'c24ba7a7fe424b96ed72f7f49c10bf9a'
	}
};

const CategoryType = new GraphQLObjectType({
	name: 'Category',
	fields:() => ({
		id: { type: GraphQLInt },
		name: { type: GraphQLString }
	})
});

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		categories: {
            type: new GraphQLList(CategoryType),
            resolve(parentValue, args) {

					return fetch('https://developers.zomato.com/api/v2.1/categories', OPTIONS)
							.then(response => response.json())
							.then(data => normalizeData(data))
            }
        }
	}
});

let normalizeData = (data) => {
	return data.categories.map(category => category.categories);
};

module.exports = new GraphQLSchema({
	query: RootQuery
});