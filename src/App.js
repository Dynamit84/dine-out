import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import categoriesQuery from './categories.graphql';
import * as images from './images';
import './App.css';

class App extends Component {
  render() {
  	const {data} = this.props;
  	if (data.loading) {
  		return <h1>Data is loading...</h1>
	}
    return (
      <div className="app">
        <header className="app-header">
          <div className="">
             <img src={images.logo} alt="logo" />
          </div>
          <h1 className="App-title">Find the best restaurants, cafés, and bars in Porto with Dine-Out!</h1>
        </header>
        <ul className='categories-list'>
			{
				data.categories.map((category, i) => {
				    let src = `category_${category.id}`;

					if (category.id === 4) {

					    return null;
                    }

					return <li key={i} className='list-item'><img src={images[src]} alt=""/><span>{category.name}</span></li>
				})
			}
		</ul>
      </div>
    );
  }
}

export default graphql(categoriesQuery)(App);
