import category_1 from './img/category_1.png';
import category_2 from './img/category_2.png';
import category_3 from './img/category_3.png';
import category_5 from './img/category_5.png';
import category_6 from './img/category_6.png';
import category_7 from './img/category_7.png';
import category_8 from './img/category_8.png';
import category_9 from './img/category_9.png';
import category_10 from './img/category_10.png';
import category_11 from './img/category_11.png';
import category_12 from './img/category_12.png';
import category_13 from './img/category_13.png';
import category_14 from './img/category_14.png';
import logo from './img/logo.png';

export {
    category_1,
    category_2,
    category_3,
    category_5,
    category_6,
    category_7,
    category_8,
    category_9,
    category_10,
    category_11,
    category_12,
    category_13,
    category_14,
    logo
}
